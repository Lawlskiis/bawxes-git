using UnityEngine;
using System.Collections;

public class Grapher2 : MonoBehaviour {
	
	public enum FunctionOption
	{
		Linear,
		Exponential,
		Parabola,
		Sine,
		Ripple
	}
	
	private delegate float FunctionDelegate(Vector3 pos, float t);
	
	private static FunctionDelegate[] functionDelegates = 
	{
		Linear,
		Exponential,
		Parabola,
		Sine,
		Ripple
	};
	
	
	public FunctionOption function;
	public int resolution = 10;
	
	private int currResolution;
	
	private ParticleSystem.Particle[] points;
	
	// Use this for initialization
	void Start () {
		
		CreatePoints();
		
	}
	
	// Update is called once per frame
	void Update () {
			
		if(currResolution != resolution)
			CreatePoints();
		
		FunctionDelegate f = functionDelegates[(int)function];
		float t = Time.timeSinceLevelLoad;
		for(int i = 0; i < points.Length; i++)
		{
			Vector3 pos = points[i].position;
			pos.y = f(pos,t);
			points[i].position = pos;
			Color green = points[i].color;
			green.g = pos.y;
			points[i].color = green;
			//points[i].size = 0.3f + 0.1f * Mathf.Sin(2 * Mathf.PI * 0.01f + Time.timeSinceLevelLoad); //size sine wave
//			if(points[i].size > 0.2f)
//				points[i].size = 0.2f;
//			else if(points[i].size < 0.01f)
//				points[i].size = 0.01f;
		}
		
		particleSystem.SetParticles(points, points.Length);
		
	}
	
	private static float Linear(Vector3 pos, float t)
	{
		return pos.x;
	}
	
	private static float Exponential(Vector3 pos, float t)
	{
		return pos.x*pos.x;	
	}
	
	private static float Parabola(Vector3 pos, float t)
	{
		pos.x = 2f * pos.x - 1f;
		pos.z = 2f * pos.z - 1f;
		return 1f - pos.x*pos.x*pos.z*pos.z;
	}
	
	private static float Sine(Vector3 pos, float t)
	{
		return 0.5f +
				0.25f*Mathf.Sin(4*Mathf.PI*(pos.x+t*4))*Mathf.Sin(2*Mathf.PI*pos.z+t)+
				0.10f * Mathf.Cos(3*Mathf.PI*pos.x+5*t)*Mathf.Cos(5*Mathf.PI*pos.z + t*3)+
				0.15f * Mathf.Sin(Mathf.PI*pos.x + 0.6f*t);
	}
	
	private static float Ripple(Vector3 pos, float t)
	{
		float sqrRadius = (pos.x - 0.5f)*(pos.x - 0.5f) + (pos.z - 0.5f)*(pos.z - 0.5f);
		return 0.5f + Mathf.Sin(15*Mathf.PI * sqrRadius - 2f*t)/(2f + 100f*sqrRadius);
	}
	
	private void CreatePoints()
	{
		if(resolution > 100)
			resolution = 100;
		else if(resolution < 2)
			resolution = 2;
		
		currResolution = resolution;
		
		points = new ParticleSystem.Particle[resolution*resolution];
		float increment = 1f / (resolution - 1);
		int i = 0;
		for(int x = 0; x < resolution; x++)
		{
			for(int z = 0; z < resolution; z++)
			{
				Vector3 pos = new Vector3(x*increment, 0f, z*increment);
				points[i].position = pos;
				points[i].color = new Color(pos.x, 0f, pos.z);
				points[i++].size = 0.1f;
			}
		}
	}
}
