using UnityEngine;
using TNet;

public class InputManager : MonoBehaviour {
	
	public delegate void OnInputEvent(string input, bool down);
	public event OnInputEvent OnInput;
	public delegate void OnInputAxis(Vector3 axis);
	public event OnInputAxis OnInputAxeez;
	
	private static InputManager instance;
	private string mInput = "";
	
	private InputManager()
	{
		
	}
	
	 //  Instance  
	public static InputManager Instance
	{    
		get    
		{      
			if (instance ==  null)
			instance = GameObject.FindObjectOfType(typeof(InputManager)) as  InputManager;     
			return instance;
        }
	}
	
	
	void Awake ()
	{
		
	}
	
	void Update ()
	{
		if(Input.GetKey(KeyCode.Escape))
			Application.Quit();
		if(Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0)
		{
			if (OnInputAxeez != null)
				OnInputAxeez(new Vector3(Input.GetAxis("Horizontal"),0,Input.GetAxis("Vertical")));
		}
	}
     
	void OnGUI()
	{
		Event e = Event.current;
			string s = string.Empty;
		if (OnInput != null)
		{
			if(e.type == EventType.KeyDown)
			{
				OnInput(e.keyCode.ToString(), true);
			}
			if(e.type == EventType.KeyUp)
			{
				OnInput(e.keyCode.ToString(), false);
			}
			if(e.type == EventType.MouseDown)
			{
				switch(e.button)
				{
				case 0:
					OnInput("LeftClick", true);
					break;
				case 1:
					OnInput("RightClick", true);
					break;
				case 2:
					OnInput("MiddleClick", true);
					break;
				}
			}
			if(e.type == EventType.MouseUp)
			{
				switch(e.button)
				{
				case 0:
					OnInput("LeftClick", false);
					break;
				case 1:
					OnInput("RightClick", false);
					break;
				case 2:
					OnInput("MiddleClick", false);
					break;
				}
			}
		}
	}
}