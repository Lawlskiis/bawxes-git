using UnityEngine;
using System.Collections;

public class CursorManager : TNBehaviour {
	
	private bool isShown = false;
	
	
	void Start () 
	{
		if (tno.isMine)
		{
			InputManager.Instance.OnInput += OnInput;
			Screen.showCursor = false;
			Screen.lockCursor = true;
		}
		
	}
	
	void OnInput(string input, bool down)
	{
		if (isShown)
		{
			if(input == "V" && down == true)
			{
				CursorSwap();
			}
		}
		else
		{
			if(input == "V" && down == true)
			{
				CursorSwap();
			}
		}
	}
	
	private void CursorSwap()
	{
		if(!Screen.showCursor)
		{
			Screen.showCursor = true;
			Screen.lockCursor = false;
			isShown = true;
		}
		else
		{
			Screen.showCursor = false;
			Screen.lockCursor = true;
			isShown = false;
		}
	}
	
}
