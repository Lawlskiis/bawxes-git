using UnityEngine;
using System.Collections;
using TNet;

public class PlayerStats : TNBehaviour {

	private int mID;
	public int iD
	{
		set{tno.Send(99, TNet.Target.Host, value);}
		get{return mID;}
	}
	
	void Start() 
	{
		if (tno.isMine)
		{
			iD = TNManager.playerID;
		}
	}
	
	
	
	[RFC(99)]
	void OnSetID(int v)
	{
		mID = v;
		print(v);
	}
	
}
