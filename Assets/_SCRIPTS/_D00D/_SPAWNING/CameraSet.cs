using UnityEngine;
using System.Collections;
using TNet;

public class CameraSet  : TNBehaviour {
	
	private Camera preCam;
	private MouseOrbit playerCam;
	public Animator anim;
	public Transform meshToSet;
	
	
	
	// Use this for initialization
	void Start () {
		if (tno.isMine)
			{	
			
					GameObject camera = new GameObject("ThirdPersonCamera",typeof(Camera)) as GameObject;
					camera.AddComponent<MouseOrbit>();
					MouseOrbit camMouse = camera.GetComponent<MouseOrbit>();
					camera.camera.cullingMask = 7;
					camMouse.enabled = true;
					camMouse.target = transform;
					camMouse.camera.enabled = true;
					camMouse.animObj = anim;
					camMouse.mesh = meshToSet;
					gameObject.GetComponent<GunShooter>().cam = camera.gameObject;
					//playerCam = GameObject.Find("ThirdPersonCamera").GetComponent<MouseOrbit>();
					Camera camRef = camera.camera;
					camRef.enabled = true;
					camMouse.target = transform;
					
					if (GameObject.Find("/_MainCameraOrigin/_MainCamera"))
					{
						preCam = GameObject.Find("/_MainCameraOrigin/_MainCamera").camera;
						preCam.enabled = false;
					}
			}
	}
	
	
	
	
	
	
}
