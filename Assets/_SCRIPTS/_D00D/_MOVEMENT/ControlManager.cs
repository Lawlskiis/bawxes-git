using UnityEngine;
using TNet;

public class ControlManager : TNBehaviour 
{
	static ControlManager _instance = null;
	public static ControlManager instance
	{
		get
		{
			if(!_instance)
			{
				_instance = FindObjectOfType(typeof(ControlManager))as ControlManager;
				
				if(!_instance)
				{
					var obj = new GameObject ("_ControlManager");
					_instance = obj.AddComponent<ControlManager>();
				}
			}
			return _instance;
		}
	}
	
	public float speed = 10.0f;
	public float gravity = 10.0f;
	public float maxVelocityChange = 10.0f;
	public bool canJump = true;
	public float jumpHeight = 2.0f;
	private bool grounded = false;	
	private bool sprintCD = false;
	public float maxStam = 100f;
	public float currStam = 100f;
	public Animator animObj;
	
	
	private Vector3 mTargetVelocity = Vector3.zero;
	public Vector3 targetVelocity{set{tno.Send(4, TNet.Target.All, value);}}
	
	int mJumping = 0;
	public int jumping{set{tno.Send (5, TNet.Target.All, value);}}
	
	private int mIsSprinting = 0;
	public int isSprinting{set{tno.Send(6, TNet.Target.All, value);}}
	
	
	void Start() 
	{
		rigidbody.freezeRotation = true;
		rigidbody.useGravity = false;
		
		if(tno.isMine)
		{
			InputManager.Instance.OnInput += OnInput;
			InputManager.Instance.OnInputAxeez += OnInputAxis;
		}
	}
	
	void Update () 
	{	
		if(sprintCD)
		{
			currStam++;
			if(currStam >= 100)
				sprintCD = false;
		}
		if(mIsSprinting == 1)
		{
			if(currStam > 0 && !sprintCD)
			{
				speed = 20f;
				currStam--;
			}
			if(currStam <= 0)
			{
				sprintCD = true;
				speed = 10f;
			}
		}
		else
		{
			if(currStam < maxStam)
			{
				speed = 10f;
				currStam++;
			}
		}
	}
	
	void FixedUpdate () 
	{
		if (grounded) 
		{
			// Calculate how fast we should be moving
			Vector3 tVel = new Vector3(0, 0, 0);
			tVel = mTargetVelocity;
			tVel = transform.TransformDirection(mTargetVelocity);
			tVel *= speed;
			
			// Apply a force that attempts to reach our target velocity
			Vector3 velocity = rigidbody.velocity;
			Vector3 velocityChange = (tVel - velocity);
			velocityChange.x = Mathf.Clamp(velocityChange.x, -maxVelocityChange, maxVelocityChange);
			velocityChange.z = Mathf.Clamp(velocityChange.z, -maxVelocityChange, maxVelocityChange);
			velocityChange.y = 0;
			rigidbody.AddForce(velocityChange, ForceMode.VelocityChange);
			
			// Jump
			if (canJump && mJumping == 1) 
			{
				rigidbody.velocity = new Vector3(velocity.x, CalculateJumpVerticalSpeed(), velocity.z);
				mJumping = 0;
				canJump = false;
			}
		}
		else
		{
			// Calculate how fast we should be moving
			Vector3 tVel = new Vector3(0, 0, 0);
			tVel = mTargetVelocity;
			tVel = transform.TransformDirection(mTargetVelocity);
			tVel *= (speed/2);
			
			// Apply a force that attempts to reach our target velocity
			Vector3 velocity = rigidbody.velocity;
			Vector3 velocityChange = (tVel - velocity);
			velocityChange.x = Mathf.Clamp(velocityChange.x, -maxVelocityChange, maxVelocityChange);
			velocityChange.z = Mathf.Clamp(velocityChange.z, -maxVelocityChange, maxVelocityChange);
			velocityChange.y = 0;
			rigidbody.AddForce(velocityChange, ForceMode.VelocityChange);
		}
		
		// We apply gravity manually for more tuning control
		rigidbody.AddForce(new Vector3 (0, -gravity * rigidbody.mass, 0));
		
		grounded = false;
	}
	
	float CalculateJumpVerticalSpeed() 
	{
		// From the jump height and gravity we deduce the upwards speed 
		// for the character to reach at the apex.
		return Mathf.Sqrt(2 * jumpHeight * gravity);
	}
	
	void OnCollisionStay () 
	{
		canJump = true;
		grounded = true; 
	}
	
	void OnNetworkPlayerJoin (Player p)
	{
		tno.Send(7, p, transform.position);
	}
	
	void OnInputAxis(Vector3 input)
	{
		targetVelocity = input;
		
	}
	
	void OnInput(string input, bool down) 
	{
		if(down)
		{
			switch(input)
			{
			case "RightClick":
				isSprinting = 1;
				break;
			case "Space":
				jumping = 1;
				break;
			}
		}
		if(!down)
		{
			switch(input)
			{
			case "RightClick":
				isSprinting = 0;
				break;
			case "Space":
				jumping = 0;
				break;
			}
		}
	}
	
	[RFC(4)]
	void OnSetVel(Vector3 vel)
	{
		mTargetVelocity = vel;
		animObj.SetFloat("speed", vel.magnitude);
	}
	[RFC(5)]
	void OnSetJump(int j)
	{
		mJumping = j;
	}
	[RFC(6)]
	void OnSetSprint(int s)
	{
		mIsSprinting = s;
	}
	[RFC(7)]
	void OnSetTargetImmediate (Vector3 pos)
	{
		transform.position = pos;
	}
}
