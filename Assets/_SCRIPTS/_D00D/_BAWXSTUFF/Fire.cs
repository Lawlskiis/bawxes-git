using UnityEngine;
using System.Collections;

public class Fire : ThrowBawxes {
	
	
//private player	
//private  p = global.Instance.player;

public float smooth = 3f;		// a public variable to adjust smoothing of camera motion

public AudioClip sound;
public AudioClip sound2;

private Transform p;
public Transform standardPos;
private bool canThrow=false;
private bool canPickup=false;
private bool hasBox=false;
private int timer = 20;
private int timerMax = 20;
private bool useTimer = false;
		
	void Start()
	{
	//	standardPos = GameObject.Find("BawxPos").transform;	
	
	}
		
	void FixedUpdate()
	{
		holdBox();	
	}
		
	void Update()
	{
		if(Input.GetKeyDown(KeyCode.E) && !HasBox()) 
			startTimer();
		else if(Input.GetKeyDown(KeyCode.E) && checkFire() == true) 
		{
			performFire(); 
			stopTimer();
		}
		
		if (useTimer && timer >= 0) 
			timer--; 
		else if(useTimer) 
			stopTimer();
	}
	
	void startTimer() 
	{
		useTimer=true;
		timer=timerMax;
	}
	
	void stopTimer() 
	{
		useTimer=false;
		timer=timerMax;
	}
	
	
	void OnTriggerStay(Collider other)
	{
		if (other.gameObject.tag == "Bawx" && !HasBox() && useTimer) 
		{
			if (sound2) 
				AudioSource.PlayClipAtPoint(sound2, transform.position);
			
			p = other.transform;
			projectile = p;
			canThrow=true;
			hasBox=true;
			canPickup= false;			
		}
	}
	
	bool HasBox(){
			return hasBox;
		}
		
	void holdBox()
	{
			
		if (HasBox() == true) 
		{
			if (p && standardPos)
			{
				p.position = standardPos.position;
				p.parent = standardPos.transform;
				p.rigidbody.useGravity = false;
				p.rigidbody.AddTorque(Vector3.back * 10);
			}	
		}
	}
		
	bool checkFire()
	{
		if (!HasBox() || !canThrow) return false;
		else if (HasBox() && canThrow) return true;
		else return false;
	}
	
	void performFire()
	{
		p.parent = null;
		Fire();
		ControlManager.instance.currStam -= 20f;
		hasBox=false;
	}
	
	
}