using UnityEngine;
using System.Collections;

public class MoverBullet : MonoBehaviour {

	public AudioClip[] Sounds;
	public float Lifetime;
	public bool Projectile;
	public float Speed = 80.0f;
	public float SpeedMax = 80.0f;
	public float SpeedMin;
	public float SpeedMult = 1.0f;
	
	void Start () {
		Destroy(gameObject, Lifetime);
		if(Projectile)
			this.rigidbody.AddRelativeForce(Vector3.forward * Time.deltaTime * Speed, ForceMode.Impulse);
	}
	
	void Update () {
		if(!Projectile){
			
			this.rigidbody.AddRelativeForce( Vector3.forward * Time.deltaTime * Speed);
		}
		if(Speed < SpeedMax && Speed >= SpeedMin)
		{
			Speed += SpeedMult;
		}
	
		
	}

}
