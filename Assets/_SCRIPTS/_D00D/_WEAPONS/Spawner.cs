using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour {

public Transform Objectman;
public int timeSpawn= 0;
public int timeSpawnMax;
public int enemyCount= 0;
public int radius;


void  Start (){
if(renderer)
  renderer.enabled = false;

}

void  Update (){
   GameObject[] gos;
   gos = GameObject.FindGameObjectsWithTag("Enemy");
   timeSpawn+=1;
   if(gos.Length < enemyCount){
   int timespawnmax= timeSpawnMax;
   if(timespawnmax<=0){
   		timespawnmax = 10;
   }
   if(timeSpawn>=timespawnmax){
   	  GameObject enemyCreated= Instantiate(Objectman, transform.position+ new Vector3(Random.Range(-radius,radius),20,Random.Range(-radius,radius)), Quaternion.identity) as GameObject;

   	  enemyCreated.transform.localScale = new Vector3(Random.Range(5,20),enemyCreated.transform.localScale.x,enemyCreated.transform.localScale.x);
	  //enemyCreated.transform.localScale.y = enemyCreated.transform.localScale.x;
	  //enemyCreated.transform.localScale.z = enemyCreated.transform.localScale.x;
	  
   	  timeSpawn = 0;
   	  
   }
   }
   
}

}