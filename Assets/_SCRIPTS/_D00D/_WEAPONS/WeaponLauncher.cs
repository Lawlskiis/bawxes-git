using UnityEngine;
using UnityEngine;
using System.Collections;
using TNet;

public class WeaponLauncher : TNBehaviour {

	static public WeaponLauncher instance;
		
	public GameObject missile;
	public int noise;
	public GameObject Owner;
	public GameObject Flash;
	public int FlashSize = 4;
	public float fireRate= 0.1f;
	private float nextFireTime= 0.0f;
	private int timeFlash = 0;
	public AudioClip[] SoundGun;
	public int ForceShoot = 0;
	public int NumBullet = 0;
	public Texture2D targetlockontexture;
	public Texture2D targetlockedtexture;
	private GameObject target;
	private bool  locked;
	public float distanceLock = 200;
	public float timetolock = 2;
	private float timetolockcount = 0;
	public string targettag = "Enemy";
	public float aimdirection = 0.8f; 
	public bool  Seeker;
	private GameObject bullet;
	
		
	void Awake()
	{
		if(TNManager.isThisMyObject)
		{
			instance = this;
		}	
	}
	
	void  Start ()
	{
	}
	
	void  Update ()
	{
		if(Flash){
			if(timeFlash>0){
				timeFlash-=1;
			}else{
				Flash.renderer.enabled = false;
				if(Flash.light)
				Flash.light.enabled = false;
			}
		}
	}
		
	public void  Shoot ()
	{
		
	    if (Time.time > nextFireTime + fireRate){
	        
	    	nextFireTime = Time.time;
			if(SoundGun.Length>0){
				AudioSource.PlayClipAtPoint(SoundGun[Random.Range(0,SoundGun.Length)],transform.position);
			}
		
			for(int i=0;i<NumBullet;i++){
		
				Vector3 noisGun= new Vector3(Random.Range(-noise,noise),Random.Range(-noise,noise),Random.Range(-noise,noise))/200;
	   			Vector3 direction= transform.TransformDirection(Vector3.forward+noisGun);
	    		Quaternion rotate = this.transform.rotation;
	    
				//GameObject bullet = Instantiate(missile, this.transform.position, rotate) as GameObject;
				TNManager.Create(missile, this.transform.position, rotate);
				//missile
				
				if(missile.GetComponent<MoverMissile>()){
					missile.GetComponent<MoverMissile>().target = target;
				}
				if(missile.GetComponent<Damage>()){
					missile.GetComponent<Damage>().Owner = Owner;
				}
				if(missile.rigidbody){
	    			missile.rigidbody.AddForce(direction*ForceShoot);
	    		}
	    	}
	
	    	timeFlash = 1;
	    	if(Flash){
	    		Flash.renderer.enabled = true;
	    		if(Flash.light)
	    		Flash.light.enabled = true;
					
				Flash.transform.localScale = new Vector3(Random.Range(FlashSize/2,FlashSize)/100.0f,Flash.transform.localScale.x,Flash.transform.localScale.x);
				//Flash.transform.localScale.y = Flash.transform.localScale.x;
				//Flash.transform.localScale.z = Flash.transform.localScale.x;
			}
			nextFireTime += fireRate;
	    }
	    
	}

}