using UnityEngine;
using System.Collections;

public class ExplosionObject : MonoBehaviour {

public Vector3 Force;
public GameObject myObj;
public int Num;
public int Scale = 20;
public AudioClip[] Sounds;
public float LifeTimeObject = 2;
	
	
void  Start (){

	if(Sounds.Length>0){
		AudioSource.PlayClipAtPoint(Sounds[Random.Range(0,Sounds.Length)],transform.position);
	}
	if(myObj){
	for(int i=0;i<Num;i++){
		Vector3 pos= new Vector3(Random.Range(-10,10),Random.Range(-10,20),Random.Range(-10,10)) / 10f;
		TNManager.Create(myObj, this.transform.position + pos, transform.rotation);
		GameObject obj = myObj;
		float scale= Random.Range(1,Scale);
		TNManager.Destroy(myObj,LifeTimeObject);
		if(Scale>0)
		obj.transform.localScale = new Vector3(scale,scale,scale);
		if(obj.rigidbody ){
   			obj.rigidbody.AddForce(new Vector3(Random.Range(-Force.x,Force.x),Random.Range(-Force.y,Force.y),Random.Range(-Force.z,Force.z)));
   		}
   		
  		}
  	}
}

}