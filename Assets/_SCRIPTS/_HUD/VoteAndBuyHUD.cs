using UnityEngine;
using System.Collections;

public class VoteAndBuyHUD : MonoBehaviour {
	
	public Texture2D VoteMenuHUD, TopT0,MidT0,BotT0,TopT1,MidT1,BotT1, TopT2, TopT3, MidT2, MidT3, BotT2, BotT3, TopLocked,MidLocked,
						BotLocked, TopCheck, MidCheck,BotCheck,TopUnlocked,MidUnlocked,BotUnlocked, TopMouseOver,MidMouseOver,BotMouseOver,
						TopSelected,MidSelected,BotSelected;
	
	private int topTier,midTier,botTier;
	
	private bool topLocked, midLocked, botLocked, topSelected, midSelected, botSelected, votePressed;
	
	private Vector2 mousePos;
	
	// Use this for initialization
	void Start () {
	
		topTier = 1;
		midTier = 1;
		botTier = 1;
		
	}
	
	void OnGUI()
	{
		if(votePressed)
		{
			mousePos = new Vector2 (Input.mousePosition.x,(Screen.height-Input.mousePosition.y));
			GUI.DrawTexture(new Rect(Screen.width - VoteMenuHUD.width,(Screen.height*1/5),VoteMenuHUD.width, VoteMenuHUD.height),VoteMenuHUD);
			
			if(!topLocked)
			{
				if(!topSelected)
				{
					if(new Rect(Screen.width - (TopUnlocked.width*1.8f),(Screen.height*1/5)+TopUnlocked.height,TopUnlocked.width, TopUnlocked.height).Contains(mousePos))
					{
						if(GUI.Button(new Rect(Screen.width - (TopUnlocked.width*1.8f),(Screen.height*1/5)+TopUnlocked.height,TopMouseOver.width, TopMouseOver.height),TopMouseOver))
						{
							topSelected = true;
							midSelected = false;
						}
					}
					else
					{
						GUI.DrawTexture(new Rect(Screen.width - (TopUnlocked.width*1.8f),(Screen.height*1/5)+TopUnlocked.height,TopUnlocked.width, TopUnlocked.height),TopUnlocked);
					}	
				}
				else
				{
					if(GUI.Button(new Rect(Screen.width - (TopUnlocked.width*1.8f),(Screen.height*1/5)+TopUnlocked.height,TopSelected.width, TopSelected.height),TopSelected))
					{
						topSelected = false;
						midSelected = false;
					}
				}
			}
			else
			{
				GUI.DrawTexture(new Rect(Screen.width - (TopUnlocked.width*1.8f),(Screen.height*1/5)+TopUnlocked.height,TopLocked.width, TopLocked.height),TopLocked);
			}
			
			if(topTier == 0)
			{
				if(GUI.Button(new Rect(Screen.width - TopT0.width*1.5f,(Screen.height*1/5+TopT0.height),TopT0.width, TopT0.height),TopT0))
				{	
					topTier = 1;
					topLocked = false;
				}
			}
				
			else if(topTier == 1)
			{
				if(GUI.Button(new Rect(Screen.width - TopT1.width*1.5f,(Screen.height*1/5+TopT0.height),TopT1.width, TopT1.height),TopT1))
				{	
					topTier = 2;
				}
			}	
			else if(topTier == 2)
			{
				if(GUI.Button(new Rect(Screen.width - TopT2.width*1.5f,(Screen.height*1/5+TopT0.height),TopT2.width, TopT2.height),TopT2))
				{
					topTier = 3;
				}
			}
			else if(topTier == 3)
			{
				GUI.DrawTexture(new Rect(Screen.width - TopT3.width*1.5f,(Screen.height*1/5+TopT0.height),TopT3.width, TopT3.height),TopT3);
				//{
					//topLocked = true;
				//}
			}
			
			if(!midLocked)
			{
				if(!midSelected)
				{
					if(new Rect(Screen.width - (MidUnlocked.width*1.8f),(Screen.height*1/5)+MidUnlocked.height*2.2f,MidUnlocked.width, MidUnlocked.height).Contains(mousePos))
					{
						if(GUI.Button(new Rect(Screen.width - (MidUnlocked.width*1.8f),(Screen.height*1/5)+MidUnlocked.height*2.2f,MidMouseOver.width, MidMouseOver.height),MidMouseOver))
						{
							midSelected = true;
							topSelected = false;
						}
					}
					else
					{
						GUI.DrawTexture(new Rect(Screen.width - (MidUnlocked.width*1.8f),(Screen.height*1/5)+MidUnlocked.height*2.2f,MidUnlocked.width, MidUnlocked.height),MidUnlocked);
					}	
				}
				else
				{
					if(GUI.Button(new Rect(Screen.width - (MidUnlocked.width*1.8f),(Screen.height*1/5)+MidUnlocked.height*2.2f,MidSelected.width, MidSelected.height),MidSelected))
					{
						midSelected = false;
						topSelected = false;
					}
				}
			}
			else
			{
				GUI.DrawTexture(new Rect(Screen.width - (MidUnlocked.width*1.8f),(Screen.height*1/5)+MidUnlocked.height*2.2f,MidLocked.width, MidLocked.height),MidLocked);
			}
			
			if(midTier == 0)
			{
				if(GUI.Button(new Rect(Screen.width - MidT0.width*1.5f,(Screen.height*1/5+MidT0.height*2.2f),MidT0.width, MidT0.height),MidT0))
				{	
					midTier = 1;
					midLocked = false;
				}
			}
				
			else if(midTier == 1)
			{
				if(GUI.Button(new Rect(Screen.width - MidT1.width*1.5f,(Screen.height*1/5+MidT1.height*2.2f),MidT1.width, MidT1.height),MidT1))
				{	
					midTier = 2;
				}
			}	
			else if(midTier == 2)
			{
				if(GUI.Button(new Rect(Screen.width - MidT2.width*1.5f,(Screen.height*1/5+MidT2.height*2.2f),MidT2.width, MidT2.height),MidT2))
				{
					midTier = 3;
				}
			}
			else if(midTier == 3)
			{
				GUI.DrawTexture(new Rect(Screen.width - MidT3.width*1.5f,(Screen.height*1/5+MidT3.height*2.2f),MidT3.width, MidT3.height),MidT3);
				//{
				//	midLocked = true;
				//}
			}
			/*
			if(!botLocked)
			{
				if(!botSelected)
				{
					if(new Rect(Screen.width - BotUnlocked.width,(Screen.height*1/4),BotUnlocked.width, BotUnlocked.height).Contains(mousePos))
					{
						if(GUI.DrawTexture(new Rect(Screen.width - BotMouseOver.width,(Screen.height*1/4),BotMouseOver.width, BotMouseOver.height),BotMouseOver))
						{
							botSelected = true;
						}
					}
					else
					{
						GUI.DrawTexture(new Rect(Screen.width - BotUnlocked.width,(Screen.height*1/4),BotUnlocked.width, BotUnlocked.height),BotUnlocked);
					}	
				}
				else
				{
					if(GUI.DrawTexture(new Rect(Screen.width - BotSelected.width,(Screen.height*1/4),BotSelected.width, BotSelected.height),BotSelected))
					{
						botSelected = false;
					}
				}
			}
			else
			{
				GUI.DrawTexture(new Rect(Screen.width - BotLocked.width,(Screen.height*1/4),BotLocked.width, BotLocked.height),BotLocked);
			}
			
			if(botTier == 0)
			{
				if(GUI.DrawTexture(new Rect(Screen.width - BotT0.width,(Screen.height*1/4),BotT0.width, BotT0.height),BotT0))
				{	
					botTier = 1;
					botLocked = false;
				}
			}
				
			else if(botTier == 1)
			{
				if(GUI.DrawTexture(new Rect(Screen.width - BotT1.width,(Screen.height*1/4),BotT1.width, BotT1.height),BotT1))
				{	
					botTier = 2;
				}
			}	
			else if(botTier == 2)
			{
				if(GUI.DrawTexture(new Rect(Screen.width - BotT2.width,(Screen.height*1/4),BotT2.width, BotT2.height),BotT2))
				{
					botTier = 3;
				}
			}
			else if(botTier == 3)
			{
				if(GUI.DrawTexture(new Rect(Screen.width - BotT3.width,(Screen.height*1/4),BotT3.width, BotT3.height),BotT3))
				{
					botLocked = true;
				}
			}
			
			*/
		}
		
	}
	
	// Update is called once per frame
	void Update () {
	
		if(Input.GetKeyDown(KeyCode.V))
		{
			if(!votePressed)
			{
				votePressed = true;
			}
			else
			{
				votePressed = false;
			}
		}
		
	}
}
