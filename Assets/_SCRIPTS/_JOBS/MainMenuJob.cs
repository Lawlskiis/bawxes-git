using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.Threading;

public class MainMenuJob
{
	public event System.Action<bool> jobComplete;
	
	private bool _running;
	public bool running { get { return _running; } }
	
	private bool _paused;
	public bool paused { get { return _paused; } }


	private IEnumerator _coroutine;
	private bool _jobWasKilled;
	private Stack<MainMenuJob> _childMainMenuJobStack;


	#region constructors
	
	public MainMenuJob( IEnumerator coroutine ) : this( coroutine, true )
	{}

	
	public MainMenuJob( IEnumerator coroutine, bool shouldStart )
	{
		_coroutine = coroutine;
		
		if( shouldStart )
			start();
	}

	#endregion
	
	
	#region static MainMenuJob makers
	
	public static MainMenuJob make( IEnumerator coroutine )
	{
		return new MainMenuJob( coroutine );
	}
	
	
	public static MainMenuJob make( IEnumerator coroutine, bool shouldStart )
	{
		return new MainMenuJob( coroutine, shouldStart );
	}
	
	#endregion

	
	#region public API
	
	public MainMenuJob createAndAddChildMainMenuJob( IEnumerator coroutine )
	{
		var j = new MainMenuJob( coroutine, false );
		addChildMainMenuJob( j );
		return j;
	}
	
	
	public void addChildMainMenuJob( MainMenuJob childMainMenuJob )
	{
		if( _childMainMenuJobStack == null )
			_childMainMenuJobStack = new Stack<MainMenuJob>();
		_childMainMenuJobStack.Push( childMainMenuJob );
	}
	
	
	public void removeChildMainMenuJob( MainMenuJob childMainMenuJob )
	{
		if( _childMainMenuJobStack.Contains( childMainMenuJob ) )
		{
			var childStack = new Stack<MainMenuJob>( _childMainMenuJobStack.Count - 1 );
			var allCurrentChildren = _childMainMenuJobStack.ToArray();
			System.Array.Reverse( allCurrentChildren );
			
			for( var i = 0; i < allCurrentChildren.Length; i++ )
			{
				var j = allCurrentChildren[i];
				if( j != childMainMenuJob )
					childStack.Push( j );
			}
			
			// assign the new stack
			_childMainMenuJobStack = childStack;
		}
	}
	

	public void start()
	{
		_running = true;
		MenuManager.instance.StartCoroutine( doWork() );
	}
	
	
	public IEnumerator startAsCoroutine()
	{
		_running = true;
		yield return MenuManager.instance.StartCoroutine( doWork() );
	}
	
	
	public void pause()
	{
		_paused = true;
	}
	
	
	public void unpause()
	{
		_paused = false;
	}
	
	
	public void kill()
	{
		_jobWasKilled = true;
		_running = false;
		_paused = false;
	}
	
	
	public void kill( float delayInSeconds )
	{
		var delay = (int)( delayInSeconds * 1000 );
		new System.Threading.Timer( obj =>
		{
			lock( this )
			{
				kill();
			}
		}, null, delay, System.Threading.Timeout.Infinite );
	}
	
	#endregion


	private IEnumerator doWork()
	{
		// null out the first run through in case we start paused
		yield return null;
		
		while( _running )
		{
			if( _paused )
			{
				yield return null;
			}
			else
			{
				// run the next iteration and stop if we are done
				if( _coroutine.MoveNext() )
				{
					yield return _coroutine.Current;
				}
				else
				{
					// run our child jobs if we have any
					if( _childMainMenuJobStack != null )
						yield return MenuManager.instance.StartCoroutine( runChildMainMenuJobs() );
					_running = false;
				}
			}
		}
		
		// fire off a complete event
		if( jobComplete != null )
		{
			jobComplete( _jobWasKilled );
		}
	}


	private IEnumerator runChildMainMenuJobs()
	{
		if( _childMainMenuJobStack != null && _childMainMenuJobStack.Count > 0 )
		{
			do
			{
				MainMenuJob childMainMenuJob = _childMainMenuJobStack.Pop();
				yield return MenuManager.instance.StartCoroutine( childMainMenuJob.startAsCoroutine() );
			}
			while( _childMainMenuJobStack.Count > 0 && _running );
		}
	}


}