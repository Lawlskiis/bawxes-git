using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.Threading;

public class SelectionMenuJob
{
	public event System.Action<bool> jobComplete;
	
	private bool _running;
	public bool running { get { return _running; } }
	
	private bool _paused;
	public bool paused { get { return _paused; } }


	private IEnumerator _coroutine;
	private bool _jobWasKilled;
	private Stack<SelectionMenuJob> _childSelectionMenuJobStack;


	#region constructors
	
	public SelectionMenuJob( IEnumerator coroutine ) : this( coroutine, true )
	{}

	
	public SelectionMenuJob( IEnumerator coroutine, bool shouldStart )
	{
		_coroutine = coroutine;
		
		if( shouldStart )
			start();
	}

	#endregion
	
	
	#region static SelectionMenuJob makers
	
	public static SelectionMenuJob make( IEnumerator coroutine )
	{
		return new SelectionMenuJob( coroutine );
	}
	
	
	public static SelectionMenuJob make( IEnumerator coroutine, bool shouldStart )
	{
		return new SelectionMenuJob( coroutine, shouldStart );
	}
	
	#endregion

	
	#region public API
	
	public SelectionMenuJob createAndAddChildSelectionMenuJob( IEnumerator coroutine )
	{
		var j = new SelectionMenuJob( coroutine, false );
		addChildSelectionMenuJob( j );
		return j;
	}
	
	
	public void addChildSelectionMenuJob( SelectionMenuJob childSelectionMenuJob )
	{
		if( _childSelectionMenuJobStack == null )
			_childSelectionMenuJobStack = new Stack<SelectionMenuJob>();
		_childSelectionMenuJobStack.Push( childSelectionMenuJob );
	}
	
	
	public void removeChildSelectionMenuJob( SelectionMenuJob childSelectionMenuJob )
	{
		if( _childSelectionMenuJobStack.Contains( childSelectionMenuJob ) )
		{
			var childStack = new Stack<SelectionMenuJob>( _childSelectionMenuJobStack.Count - 1 );
			var allCurrentChildren = _childSelectionMenuJobStack.ToArray();
			System.Array.Reverse( allCurrentChildren );
			
			for( var i = 0; i < allCurrentChildren.Length; i++ )
			{
				var j = allCurrentChildren[i];
				if( j != childSelectionMenuJob )
					childStack.Push( j );
			}
			
			// assign the new stack
			_childSelectionMenuJobStack = childStack;
		}
	}
	

	public void start()
	{
		_running = true;
		SelectionManager.instance.StartCoroutine( doWork() );
	}
	
	
	public IEnumerator startAsCoroutine()
	{
		_running = true;
		yield return SelectionManager.instance.StartCoroutine( doWork() );
	}
	
	
	public void pause()
	{
		_paused = true;
	}
	
	
	public void unpause()
	{
		_paused = false;
	}
	
	
	public void kill()
	{
		_jobWasKilled = true;
		_running = false;
		_paused = false;
	}
	
	
	public void kill( float delayInSeconds )
	{
		var delay = (int)( delayInSeconds * 1000 );
		new System.Threading.Timer( obj =>
		{
			lock( this )
			{
				kill();
			}
		}, null, delay, System.Threading.Timeout.Infinite );
	}
	
	#endregion


	private IEnumerator doWork()
	{
		// null out the first run through in case we start paused
		yield return null;
		
		while( _running )
		{
			if( _paused )
			{
				yield return null;
			}
			else
			{
				// run the next iteration and stop if we are done
				if( _coroutine.MoveNext() )
				{
					yield return _coroutine.Current;
				}
				else
				{
					// run our child jobs if we have any
					if( _childSelectionMenuJobStack != null )
						yield return SelectionManager.instance.StartCoroutine( runChildSelectionMenuJobs() );
					_running = false;
				}
			}
		}
		
		// fire off a complete event
		if( jobComplete != null )
		{
			jobComplete( _jobWasKilled );
		}
	}


	private IEnumerator runChildSelectionMenuJobs()
	{
		if( _childSelectionMenuJobStack != null && _childSelectionMenuJobStack.Count > 0 )
		{
			do
			{
				SelectionMenuJob childSelectionMenuJob = _childSelectionMenuJobStack.Pop();
				yield return SelectionManager.instance.StartCoroutine( childSelectionMenuJob.startAsCoroutine() );
			}
			while( _childSelectionMenuJobStack.Count > 0 && _running );
		}
	}


}