using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NetworkManager : MonoBehaviour 
{
	private static NetworkManager instance;
	public static NetworkManager Instance
	{
		get
		{
			if (instance == null)
			{
				instance = new GameObject("NetworkManager").AddComponent<NetworkManager> ();
			}
			return instance;
		}
	}
	
	[System.NonSerialized]
	public bool disconnecting = false;
	[System.NonSerialized]
	public int playerNum = 0;
	[System.NonSerialized]
	public int serverNum = 0;
	[System.NonSerialized]
	public int serverPort;
	[System.NonSerialized]
	public float refreshTime;
	[System.NonSerialized]
	public string lobbyName = "Party Time";
	public HostData[] hostData;
	private bool refreshing = false;
	
	private bool isServerStarted = false;
	
	private string masterServerUrl = "lawlskiis.no-ip.biz";
	
	void Awake ()
	{
		DontDestroyOnLoad(this);
		instance = this;
		Application.runInBackground = true;
	}
	
	void Start ()
	{
		
	}
	
	public void StartServer(int portIn)
	{
		serverPort = portIn;
		Network.InitializeServer( 17, portIn, !Network.HavePublicAddress() );
		MasterServer.RegisterHost("BAWXES", lobbyName, "CTF FPS RTS");
	}
	
	public void StopServer()
	{
		Network.maxConnections = 68;
		RefreshHostList();
		disconnecting = true;
		isServerStarted = false;
	}
	
	[RPC]
	public void RefreshHostList()
	{
		MasterServer.RequestHostList("BAWXES");
		refreshing = true;
		refreshTime = 10.0F;	
	}
	
	void Update () 
	{
		if ( refreshing ) 
		{
			refreshTime -= Time.deltaTime;
			if ( MasterServer.PollHostList().Length != 0 ) 
			{
				refreshing = false;
				refreshTime = 0.0F;
				hostData = MasterServer.PollHostList();
				UpdateServerNum();
				if ( disconnecting && hostData[serverNum].playerLimit == 69 ) 
				{
					disconnecting = false;
					Network.Disconnect();
					isServerStarted = false;
				}
			}
			if ( refreshTime <= 0.0F ) 
			{
				if ( MasterServer.PollHostList().Length == 0 )
				{
					print( "No hosts found..." );
				}
				refreshing = false;
				refreshTime = 0.0F;
			}
		}
	}
	
	void OnServerInitialized() 
	{
		isServerStarted = true;
	}

	void OnConnectedToServer() 
	{
		networkView.RPC("RefreshHostList", RPCMode.AllBuffered);
	}
	
	void OnDisconnectedFromServer() 
	{
		isServerStarted = false;
		networkView.RPC("RefreshHostList", RPCMode.AllBuffered);
		networkView.RPC("UpdatePlayerNums", RPCMode.All, playerNum);
	}
	
	void OnFailedToConnectToMasterServer(NetworkConnectionError info) 
	{
        Debug.Log("Could not connect to master server: " + info);
		MasterServer.RequestHostList("BAWXES");
    }
	
	public int GetHostLength()
	{
		int retLength = 0;
	
		if(MasterServer.PollHostList().Length != 0) 
		{
			if ( hostData.Length > 0 )
			{
				retLength = hostData.Length;
			}
		}
		return retLength;	
	}
	
	public void UpdateServerNum()
	{
		for ( int fCtr = 0; fCtr < hostData.Length; fCtr++ ) 
		{
			if ( serverPort == hostData[fCtr].port )
			{
				serverNum = fCtr;
			}
		}	
	}
	
	[RPC]
	public void UpdatePlayerNums( int indexIn ) 
	{
		if ( playerNum > indexIn )
		{
			playerNum--;
		}
	}
	
	public void OnApplicationQuit ()
	{
		instance = null;
	}
}
