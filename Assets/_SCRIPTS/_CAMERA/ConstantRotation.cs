using UnityEngine;
using System.Collections;

public class ConstantRotation : MonoBehaviour {
	
	public float speed = 1.0F;

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{	
		transform.Rotate(0, speed * Time.deltaTime, 0);
	}
}
