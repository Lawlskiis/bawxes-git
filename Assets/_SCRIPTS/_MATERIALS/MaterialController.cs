using UnityEngine;
using System.Collections;

public class MaterialController : MonoBehaviour {

	public void to1()
	{
		this.renderer.material.SetFloat("_Blend", 0);
	}
	
	public void to2()
	{
		this.renderer.material.SetFloat("_Blend", 1);
	}
}
