using UnityEngine;
using TNet;

public class Item : TNBehaviour
{
	private string mName;
	public string name
	{
		set{mName = value;}
		get{return mName;}
	}
	private int mTier = 0;
	public int tier
	{
		set{tno.Send(51, TNet.Target.AllSaved, value);}
		get{return mTier;}
	}
	private int mBawxCost;
	public int bawxCost
	{
		set{tno.Send(52, TNet.Target.AllSaved, value);}
		get{return mBawxCost;}
	}
	private int mPersonalCost;
	public int personalCost
	{
		set{tno.Send(53, TNet.Target.AllSaved, value);}
		get{return mPersonalCost;}
	}
	private bool mUnlocked = false;
	public bool unlocked
	{
		set{tno.Send(54, TNet.Target.AllSaved, value);}
		get{return mUnlocked;}
	}
	private int mVotes = 0;
	public int votes
	{
		set{tno.Send(55, TNet.Target.AllSaved, value);}
		get{return mVotes;}
	}
	
	[RFC(51)]
	void OnSetTier(int v)
	{
		mTier = tier;
	}
	
	[RFC(52)]
	void OnSetBawxCost(int v)
	{
		mBawxCost = v;
	}
	
	[RFC(53)]
	void OnSetPersonalCost(int v)
	{
		mPersonalCost = v;
	}
	
	[RFC(54)]
	void OnSetUnlocked(bool v)
	{
		mUnlocked = v;
	}
	
	[RFC(54)]
	void OnSetVotes(int v)
	{
		mVotes = v;
	}
}