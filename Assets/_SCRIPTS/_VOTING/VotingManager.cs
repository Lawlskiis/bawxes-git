using UnityEngine;
using TNet;

public class VotingManager : TNBehaviour {
	
	static VotingManager _instance = null;
	public static VotingManager instance
	{
		get
		{
			if(!_instance)
			{
				_instance = FindObjectOfType(typeof(VotingManager))as VotingManager;
				
				if(!_instance)
				{
					var obj = new GameObject ("_VotingManager");
					_instance = obj.AddComponent<VotingManager>();
				}
			}
			return _instance;
		}
	}
	
	private int mWarmBawxes = 0;
	public int warmBawxes
	{
		set{tno.Send(51, TNet.Target.AllSaved, value);}
		get{return mWarmBawxes;}
	}
	private int mColdBawxes = 0;
	public int coldBawxes
	{
		set{tno.Send(52, TNet.Target.AllSaved, value);}
		get{return mColdBawxes;}
	}
	
	GameObject[] warmWeaponObjects;
	GameObject[] coldWeaponObjects;
	GameObject warmWeaponsParentObject;
	GameObject coldWeaponsParentObject;
	Item[] warmWeapons;
	Item[] coldWeapons;
	
	void Awake ()
	{
		warmWeaponObjects = GameObject.FindGameObjectsWithTag("WarmWeapon");
		coldWeaponObjects = GameObject.FindGameObjectsWithTag("ColdWeapon");
		warmWeaponsParentObject = GameObject.Find("/_Weapons/WarmWeapons");
		coldWeaponsParentObject = GameObject.Find("/_Weapons/ColdWeapons");
		
		foreach(GameObject go in warmWeaponObjects)
		{
			Item goI = go.GetComponent<Item>();
			switch(go.name)
			{
			case "CardGun":
				goI.name = "CardGun";
				break;
			case "GrenadeLadel":
				goI.name = "GrenadeLadel";
				break;
			case "MilkLauncher":
				goI.name = "MilkLauncher";
				break;
			case "StapleGun":
				goI.name = "StapleGun";
				break;
			}
		}
		foreach(GameObject go in coldWeaponObjects)
		{
			Item goI = go.GetComponent<Item>();
			switch(go.name)
			{
			case "CardGun":
				goI.name = "CardGun";
				break;
			case "GrenadeLadel":
				goI.name = "GrenadeLadel";
				break;
			case "MilkLauncher":
				goI.name = "MilkLauncher";
				break;
			case "StapleGun":
				goI.name = "StapleGun";
				break;
			}
		}
	}
	
	void Start () 
	{
		if(TNManager.isHosting)
		{
			warmWeapons = warmWeaponsParentObject.GetComponentsInChildren<Item>();
			coldWeapons = coldWeaponsParentObject.GetComponentsInChildren<Item>();
			
			foreach(Item i in warmWeapons)
			{
				switch(i.name)
				{
				case "CardGun":
					i.bawxCost = 5;
					i.personalCost = 0;
					break;
				case "GrenadeLadel":
					i.bawxCost = 6;
					i.personalCost = 0;
					break;
				case "MilkLauncher":
					i.bawxCost = 6;
					i.personalCost = 0;
					break;
				case "StapleGun":
					i.bawxCost = 4;
					i.personalCost = 0;
					break;
				}
			}
			foreach(Item i in coldWeapons)
			{
				switch(i.name)
				{
				case "CardGun":
					i.bawxCost = 5;
					i.personalCost = 0;
					break;
				case "GrenadeLadel":
					i.bawxCost = 6;
					i.personalCost = 0;
					break;
				case "MilkLauncher":
					i.bawxCost = 6;
					i.personalCost = 0;
					break;
				case "StapleGun":
					i.bawxCost = 4;
					i.personalCost = 0;
					break;
				}
			}
		}
	}
	
	void Vote(string team, string item)
	{
		if(team == "Warm")
		{
			
		}
		if(team == "Cold")
		{
			
		}
	}
	
	void Update () 
	{
		
	}
}
