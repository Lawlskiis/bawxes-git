using UnityEngine;
using System.Collections;
using TNet;

public class SpawningManager : TNBehaviour {
	
	//Borris Baboon
	//Sheldon the Turle
	//Scraps the Cat
	//Patrick the Platypus
	//Rex the Rhino
	//Orville the Owl
	
	static SpawningManager _instance = null;
	public static SpawningManager instance
	{
		get
		{
			if(!_instance)
			{
				_instance = FindObjectOfType(typeof(SpawningManager))as SpawningManager;
				
				if(!_instance)
				{
					var obj = new GameObject ("_SpawningManager");
					_instance = obj.AddComponent<SpawningManager>();
				}
			}
			return _instance;
		}
	}
	private HostJoinGUI hostJoinGUI;
	
	public GameObject warmSpawn;
	public GameObject warmBorris;
	public GameObject warmSheldon;
	public GameObject warmScraps;
	public GameObject warmPatrick;
	public GameObject warmRex;
	public GameObject warmOrville;
	
	public GameObject coldSpawn;
	public GameObject coldBorris;
	public GameObject coldSheldon;
	public GameObject coldScraps;
	public GameObject coldPatrick;
	public GameObject coldRex;
	public GameObject coldOrville;
	
	GameObject[] warmSpawnPoints;
	GameObject[] coldSpawnPoints;
	
	private string mTeam;
	private int mSpawnPosition;
	private string mCharacter;
	
	void Awake () {
		
		//DontDestroyOnLoad(this);
		
		hostJoinGUI = GameObject.Find("/TNManager").GetComponent<HostJoinGUI>();
		
		warmSpawnPoints = GameObject.FindGameObjectsWithTag("WarmSpawns");
		coldSpawnPoints = GameObject.FindGameObjectsWithTag("ColdSpawns");
		
		if(warmSpawnPoints.Length != 0)
		{
			foreach(GameObject go in warmSpawnPoints)
			{
				SpawnPoint sp = go.GetComponent<SpawnPoint>();
				sp.position = go.transform.position;
				sp.rotation = go.transform.rotation;
			}
		}
		if(coldSpawnPoints.Length != 0)
		{
			foreach(GameObject go in coldSpawnPoints)
			{
				SpawnPoint sp = go.GetComponent<SpawnPoint>();
				sp.position = go.transform.position;
				sp.rotation = go.transform.rotation;
			}
		}
	}
	
	void Start () {
	
		hostJoinGUI.OnDisconnect += OnPlayerDisconnect;
		
	}
	
	void OnPlayerDisconnect(int id)
	{
		foreach(GameObject go in warmSpawnPoints)
		{
			if(id == go.GetComponent<SpawnPoint>().iD)
			{
				go.GetComponent<SpawnPoint>().taken = false;
			}
		}
		foreach(GameObject go in coldSpawnPoints)
		{
			if(id == go.GetComponent<SpawnPoint>().iD)
			{
				go.GetComponent<SpawnPoint>().taken = false;
			}
		}
	}
	
	int CheckForWarmSpawn()
	{
		for(int i = 0; i < warmSpawnPoints.Length; i++)
		{
			if(warmSpawnPoints[i].GetComponent<SpawnPoint>().taken == false)
			{
				return i;	
			}
		}
		return 69;
	}
	
	int CheckForColdSpawn()
	{
		for(int i = 0; i < coldSpawnPoints.Length; i++)
		{
			if(coldSpawnPoints[i].GetComponent<SpawnPoint>().taken == false)
			{
				return i;	
			}
		}
		return 69;
	}
	
	public IEnumerator CallRespawn()
	{	
		yield return new WaitForSeconds(1);
		Respawn();
	}
	
	private void Respawn()
	{
		if(mTeam == "Warm")
		{
			switch(mCharacter)
			{
			case "Borris":
				TNManager.Create(warmBorris, warmSpawnPoints[mSpawnPosition].GetComponent<SpawnPoint>().position, warmSpawnPoints[mSpawnPosition].GetComponent<SpawnPoint>().rotation, false);
				break;
			case "Sheldon":
				TNManager.Create(warmSheldon, warmSpawnPoints[mSpawnPosition].GetComponent<SpawnPoint>().position, warmSpawnPoints[mSpawnPosition].GetComponent<SpawnPoint>().rotation);
				break;
			case "Scraps":
				TNManager.Create(warmScraps, warmSpawnPoints[mSpawnPosition].GetComponent<SpawnPoint>().position, warmSpawnPoints[mSpawnPosition].GetComponent<SpawnPoint>().rotation);
				break;
			case "Patrick":
				TNManager.Create(warmPatrick, warmSpawnPoints[mSpawnPosition].GetComponent<SpawnPoint>().position, warmSpawnPoints[mSpawnPosition].GetComponent<SpawnPoint>().rotation);
				break;
			case "Rex":
				TNManager.Create(warmRex, warmSpawnPoints[mSpawnPosition].GetComponent<SpawnPoint>().position, warmSpawnPoints[mSpawnPosition].GetComponent<SpawnPoint>().rotation);
				break;
			case "Orville":
				TNManager.Create(warmOrville, warmSpawnPoints[mSpawnPosition].GetComponent<SpawnPoint>().position, warmSpawnPoints[mSpawnPosition].GetComponent<SpawnPoint>().rotation);
				break;
			}
		}
		if(mTeam == "Cold")
		{
			switch(mCharacter)
			{
			case "Borris":
				TNManager.Create(coldBorris, coldSpawnPoints[mSpawnPosition].GetComponent<SpawnPoint>().position, coldSpawnPoints[mSpawnPosition].GetComponent<SpawnPoint>().rotation, false);
				break;
			case "Sheldon":
				TNManager.Create(coldSheldon, coldSpawnPoints[mSpawnPosition].GetComponent<SpawnPoint>().position, coldSpawnPoints[mSpawnPosition].GetComponent<SpawnPoint>().rotation);
				break;
			case "Scraps":
				TNManager.Create(coldScraps, coldSpawnPoints[mSpawnPosition].GetComponent<SpawnPoint>().position, coldSpawnPoints[mSpawnPosition].GetComponent<SpawnPoint>().rotation);
				break;
			case "Patrick":
				TNManager.Create(coldPatrick, coldSpawnPoints[mSpawnPosition].GetComponent<SpawnPoint>().position, coldSpawnPoints[mSpawnPosition].GetComponent<SpawnPoint>().rotation);
				break;
			case "Rex":
				TNManager.Create(coldRex, coldSpawnPoints[mSpawnPosition].GetComponent<SpawnPoint>().position, coldSpawnPoints[mSpawnPosition].GetComponent<SpawnPoint>().rotation);
				break;
			case "Orville":
				TNManager.Create(coldOrville, coldSpawnPoints[mSpawnPosition].GetComponent<SpawnPoint>().position, coldSpawnPoints[mSpawnPosition].GetComponent<SpawnPoint>().rotation);
				break;
			}
		}
	}
	
	public void SpawnSpawnPointAndPlayer(string prefab, string team)
	{
		if(team == "Warm")
		{
			int spawnIndex = CheckForWarmSpawn();
			if(spawnIndex != 69)
			{
				mTeam = "Warm";
				mSpawnPosition = spawnIndex;
				warmSpawnPoints[mSpawnPosition].GetComponent<SpawnPoint>().iD = TNManager.playerID;
				TNManager.Create(warmSpawn, warmSpawnPoints[mSpawnPosition].GetComponent<SpawnPoint>().position, warmSpawnPoints[mSpawnPosition].GetComponent<SpawnPoint>().rotation, false);
				switch(prefab)
				{
				case "Borris":
					mCharacter = "Borris";
					TNManager.Create(warmBorris, warmSpawnPoints[spawnIndex].GetComponent<SpawnPoint>().position, warmSpawnPoints[spawnIndex].GetComponent<SpawnPoint>().rotation);
					break;
				case "Sheldon":
					TNManager.Create(warmSheldon, warmSpawnPoints[spawnIndex].GetComponent<SpawnPoint>().position, warmSpawnPoints[spawnIndex].GetComponent<SpawnPoint>().rotation);
					mCharacter = "Sheldon";
					break;
				case "Scraps":
					TNManager.Create(warmScraps, warmSpawnPoints[spawnIndex].GetComponent<SpawnPoint>().position, warmSpawnPoints[spawnIndex].GetComponent<SpawnPoint>().rotation);
					mCharacter = "Scraps";
					break;
				case "Patrick":
					TNManager.Create(warmPatrick, warmSpawnPoints[spawnIndex].GetComponent<SpawnPoint>().position, warmSpawnPoints[spawnIndex].GetComponent<SpawnPoint>().rotation);
					mCharacter = "Patrick";
					break;
				case "Rex":
					TNManager.Create(warmRex, warmSpawnPoints[spawnIndex].GetComponent<SpawnPoint>().position, warmSpawnPoints[spawnIndex].GetComponent<SpawnPoint>().rotation);
					mCharacter = "Rex";
					break;
				case "Orville":
					TNManager.Create(warmOrville, warmSpawnPoints[spawnIndex].GetComponent<SpawnPoint>().position, warmSpawnPoints[spawnIndex].GetComponent<SpawnPoint>().rotation);
					mCharacter = "Orville";
					break;
				}
				warmSpawnPoints[spawnIndex].GetComponent<SpawnPoint>().taken = true;
			}
			else
				print ("HALP");
		}
		if(team == "Cold")
		{
			int spawnIndex = CheckForColdSpawn();
			if(spawnIndex != 69)
			{
				mTeam = "Cold";
				mSpawnPosition = spawnIndex;
				coldSpawnPoints[spawnIndex].GetComponent<SpawnPoint>().iD = TNManager.playerID;
				TNManager.Create(coldSpawn, coldSpawnPoints[spawnIndex].GetComponent<SpawnPoint>().position, coldSpawnPoints[spawnIndex].GetComponent<SpawnPoint>().rotation, false);
				switch(prefab)
				{
				case "Borris":
					mCharacter = "Borris";
					TNManager.Create(coldBorris, coldSpawnPoints[spawnIndex].GetComponent<SpawnPoint>().position, coldSpawnPoints[spawnIndex].GetComponent<SpawnPoint>().rotation);
					break;
				case "Sheldon":
					TNManager.Create(coldSheldon, coldSpawnPoints[spawnIndex].GetComponent<SpawnPoint>().position, coldSpawnPoints[spawnIndex].GetComponent<SpawnPoint>().rotation);
					mCharacter = "Sheldon";
					break;
				case "Scraps":
					TNManager.Create(coldScraps, coldSpawnPoints[spawnIndex].GetComponent<SpawnPoint>().position, coldSpawnPoints[spawnIndex].GetComponent<SpawnPoint>().rotation);
					mCharacter = "Scraps";
					break;
				case "Patrick":
					TNManager.Create(coldPatrick, coldSpawnPoints[spawnIndex].GetComponent<SpawnPoint>().position, coldSpawnPoints[spawnIndex].GetComponent<SpawnPoint>().rotation);
					mCharacter = "Patrick";
					break;
				case "Rex":
					TNManager.Create(coldRex, coldSpawnPoints[spawnIndex].GetComponent<SpawnPoint>().position, coldSpawnPoints[spawnIndex].GetComponent<SpawnPoint>().rotation);
					mCharacter = "Rex";
					break;
				case "Orville":
					TNManager.Create(coldOrville, coldSpawnPoints[spawnIndex].GetComponent<SpawnPoint>().position, coldSpawnPoints[spawnIndex].GetComponent<SpawnPoint>().rotation);
					mCharacter = "Orville";
					break;
				}
				coldSpawnPoints[spawnIndex].GetComponent<SpawnPoint>().taken = true;
			}
			else
				print ("HALP");
		}
	}
}
