using UnityEngine;
using System.Collections;
using TNet;

public class SpawnPoint : TNBehaviour
{	
	private HostJoinGUI hostJoinGUI;

	private bool mTaken = false;
	public bool taken
	{
		set{tno.Send(01, TNet.Target.AllSaved, value);}
		get{return mTaken;}
	}
	private int mID;
	public int iD
	{
		set{tno.Send(02, TNet.Target.AllSaved, value);}
		get{return mID;}
	}
	
	private Vector3 mPosition;
	public Vector3 position
	{
		set{mPosition = value;}
		get{return mPosition;}
	}
	private Quaternion mRotation;
	public Quaternion rotation
	{
		set{mRotation = value;}
		get{return mRotation;}
	}
	
	[RFC(01)]
	void OnSetTaken(bool taken)
	{
		mTaken = taken;
	}
	
	[RFC(02)]
	void OnSetID(int v)
	{
		mID = v;
	}
	
	void Start()
	{
		
	}
	
}